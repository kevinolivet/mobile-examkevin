# Practical exam

This repository has the purpose to give a "mission" to the Funx company's candidates. It evaluates technical capabilities, problem solving, git management and agility.

## Minimum objectives

* Integrate a match list.
* Save match on the phone calendar.

You're free to develop other features. 

## Layout

We recommend this layout, but you can use any other. Feel free to make the best.

![Alt text](layout.png)

## API

```
http://sporting.backend.masfanatico.cl/api/match/in_competition/?competition=torneo-descentralizado&format=json
```

## IOS Rules

* Swift 3 or 4
* You may use cocoapods

## Android Rules

* Java or Kotlin
* You may use gradle

## Submitting

After you're done developing, make a pull request to this repository. It will be reviewed and then rejected, doesn't matter the results.
