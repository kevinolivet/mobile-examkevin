//
//  MatchListTableViewCell.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import UIKit

protocol RecordarButtonDelegate: class {
    func didTapRecordarButton(_ cell: MatchListTableViewCell)
}

class MatchListTableViewCell: UITableViewCell {

    @IBOutlet weak var competitionNameLabel: UILabel!
    @IBOutlet weak var dateStadiumLabel: UILabel!
    @IBOutlet weak var localTeamImageView: UIImageView!
    @IBOutlet weak var visitTeamImageView: UIImageView!
    @IBOutlet weak var localTeamNameLabel: UILabel!
    @IBOutlet weak var visitTeamNameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    weak var cellDelegate: RecordarButtonDelegate?

    @IBAction func recordarButtonTapped(_ sender: UIButton) {
        cellDelegate?.didTapRecordarButton(self)
    }
}
