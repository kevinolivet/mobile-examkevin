//
//  MatchesModel.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import Foundation

struct MatchesModel: Codable {
    let matchs: [Matchs]
    struct Matchs: Codable {
        let local_team: LocalTeam
        let visit_team: VisitTeam
        let local_goals: Int
        let visit_goals: Int
        let date: String
        let time: String
        let stadium: Stadium
        let competition: Competition
    }
}
struct LocalTeam: Codable {
    let name: String
    let image: String
}
struct VisitTeam: Codable {
    let name: String
    let image: String
}
struct Stadium: Codable {
    let name: String
}
struct Competition: Codable {
    let name: String
}
