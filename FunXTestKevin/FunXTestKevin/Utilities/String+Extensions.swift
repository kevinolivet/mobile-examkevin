//
//  String+Extensions.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/31/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import Foundation

extension String {
    func localized(comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
