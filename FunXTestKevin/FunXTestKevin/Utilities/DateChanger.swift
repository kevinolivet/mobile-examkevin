//
//  DateChanger.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import Foundation

struct DateChanger {
    
    func dateFromEndpointToLabel(dateToChange: String) -> String {
        var dateToReturn = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = NSLocale(localeIdentifier: "es_CL") as Locale
        dateFormatterPrint.dateFormat = "dd 'de' MMM"
        
        if let date = dateFormatterGet.date(from: dateToChange){
            let dateString = dateFormatterPrint.string(from: date)
            let day = dateString.prefix(upTo: String.Index(encodedOffset: 6))
            let month = dateString.suffix(from: String.Index(encodedOffset: 6)).capitalized
            dateToReturn = day + month
        } else {
            print("There was an error decoding the string")
        }
        return dateToReturn
    }
    
}
