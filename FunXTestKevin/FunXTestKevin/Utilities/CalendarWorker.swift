//
//  CalendarWorker.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/31/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import EventKit

struct CalendarWorker {
    
    // Use to check if the event already is in the calendar
    func searchForEvent(withTitle: String, forDate: Date, endDate: Date) -> Bool {
        var eventFound = false
        let store = EKEventStore()
        guard let cal = store.defaultCalendarForNewEvents else { return false }
        
        let pred = store.predicateForEvents(withStart: forDate, end: endDate, calendars:[cal])
            var events = [EKEvent]()
            store.enumerateEvents(matching:pred) { ev, stop in
                events += [ev]
                if ev.title.range(of: withTitle) != nil {
                    stop.pointee = true
                    eventFound = true
                }
            }
        return eventFound
    }
    
    // Use to schedule the event
    func createEventinTheCalendar(title:String,
                                  forDate: String,
                                  location: String,
                                  successCompletion: @escaping (_ successResult: String) -> Void,
                                  failureCompletion: @escaping (_ failureResult: String) -> Void) {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        guard let eventStartDate = dateFormatterGet.date(from: forDate) else { return }
        
        let store = EKEventStore()
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: store)
                event.title = title
                event.location = location
                event.calendar = store.defaultCalendarForNewEvents
                event.startDate = eventStartDate
                let usualMatchTimeInterval = TimeInterval(90 * 60)
                let eventEndDate = eventStartDate.addingTimeInterval(usualMatchTimeInterval)
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                if self.searchForEvent(withTitle: title, forDate: eventStartDate, endDate: eventEndDate) {
                    failureCompletion("Event already exists".localized())
                } else {
                    do {
                        try store.save(event, span: .thisEvent)
                        successCompletion("Saved to Calendar".localized())
                    } catch let error as NSError {
                        failureCompletion(error.localizedDescription)
                    }
                }
            } else {
                failureCompletion(error!.localizedDescription)
            }
        }
    }
}
