//
//  Networker.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import Foundation

struct Networker {
    
    // Dependency Injection for testing
    let session: URLSession
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    let url = "http://sporting.backend.masfanatico.cl/api/match/in_competition/?competition=torneo-descentralizado&format=json"
    
    func getMatches (successCompletion: @escaping ([MatchesModel.Matchs]?) -> (), failureCompletion: @escaping (String) -> ()) {
        var matchesArray:[MatchesModel.Matchs] = []
        
        let request = URLRequest(url: URL(string: url)!)
        
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil else {
                failureCompletion(error!.localizedDescription)
                return }
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                failureCompletion("statusCode mishap: \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
                return }
            guard let data = data else {
                failureCompletion("Error with data".localized())
                return }
            
            do {
                let websiteInformation = try JSONDecoder().decode(MatchesModel.self, from: data)
                matchesArray = websiteInformation.matchs
            } catch {
                DispatchQueue.main.async {
                    failureCompletion(error.localizedDescription)
                }
            }
            DispatchQueue.main.async {
                successCompletion(matchesArray)
            }
            }
        task.resume()
    }
    
    func downloadImage(urlString: String, completion: @escaping (Data) -> ()) {
        guard let url = URL(string: urlString) else { return }
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                completion(data)
            }
            }
        task.resume()
    }
}
