//
//  MatchListTableViewController.swift
//  FunXTestKevin
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import UIKit

class MatchListTableViewController: UITableViewController, RecordarButtonDelegate {
    
    var matchesData: [MatchesModel.Matchs]!
    var imageCache: [String: UIImage]!
    var dateChanger: DateChanger!
    var spinner: UIActivityIndicatorView!
    var networker: Networker!
    lazy var calendarWorker = {
        return CalendarWorker()
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAll()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchesData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchListCell", for: indexPath) as! MatchListTableViewCell
        cell.cellDelegate = self
        
        let matchObject = matchesData[indexPath.row]
        cell.competitionNameLabel.text = matchObject.competition.name
        let dateText = dateChanger.dateFromEndpointToLabel(dateToChange: matchObject.date)
        cell.dateStadiumLabel.text = dateText + " - " + matchObject.time.prefix(5) + "hrs" + " -  " + matchObject.stadium.name
        cell.scoreLabel.text = "\(matchObject.local_goals) - \(matchObject.visit_goals)"
        cell.localTeamNameLabel.text = matchObject.local_team.name
        cell.visitTeamNameLabel.text = matchObject.visit_team.name
        
        let localUrlString = matchObject.local_team.image
        networker.downloadImage(urlString: localUrlString) { (data) in
            self.imageCache[localUrlString] = UIImage(data: data)
            cell.localTeamImageView.image = UIImage(data: data)
        }
        
        let visitUrlString = matchObject.visit_team.image
        networker.downloadImage(urlString: visitUrlString) { (data) in
            self.imageCache[visitUrlString] = UIImage(data: data)
            cell.visitTeamImageView.image = UIImage(data: data)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - Actions
    @objc func getMoreMatches() {
        
        networker.getMatches(successCompletion: { [weak self ] (matchesData: [MatchesModel.Matchs]?) in
            guard let strongSelf = self else { return }
            if let matchesData = matchesData {
                strongSelf.matchesData = matchesData
            }
            strongSelf.animateTable()
            if strongSelf.refreshControl?.isRefreshing == true {
                strongSelf.refreshControl?.endRefreshing()
            }
            strongSelf.spinner.stopAnimating()
        }) {(errorMessage) in
            self.dismissableAlert(title: "Error".localized(), message: errorMessage, vc: self, handler: { (_) in
                if self.refreshControl?.isRefreshing == true {
                    self.refreshControl?.endRefreshing()
                }
                self.spinner.stopAnimating()
            },actionBtnText: "Close".localized(), showCancelButton: false)
        }
    }
    
    func animateTable() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableViewHeight = tableView.bounds.size.height
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            })
            delayCounter += 1
        }
    }
    
    func setUpRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.getMoreMatches), for: .valueChanged)
        self.refreshControl = refreshControl
    }
    
    func setUpSpinner() {
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.color = .black
        spinner.startAnimating()
        spinner.hidesWhenStopped = true
        tableView.backgroundView = spinner
    }
    
    func setUpTableView() {
        self.title = "LISTADO DE PARTIDOS".localized()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 225
    }
    
    func setUpAll() {
        setUpTableView()
        
        matchesData = [MatchesModel.Matchs]()
        imageCache = [String: UIImage]()
        dateChanger = DateChanger()
        networker = Networker()
        
        setUpSpinner()
        setUpRefresh()
        matchesData = [MatchesModel.Matchs]()
        getMoreMatches()
    }
    
    // MARK: - RecordarButtonDelegate methods
    func didTapRecordarButton(_ cell: MatchListTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let matchObject = matchesData[indexPath.row]
            let titleName = matchObject.local_team.name + " VS " + matchObject.visit_team.name
            let dateToAdd = matchObject.date + " " + matchObject.time
            dismissableAlert(title: "Add to Calendar?".localized(), message: "Do you want to add this match to your Calendar?".localized(), vc: self, handler: { (_) in
                self.calendarWorker.createEventinTheCalendar(title: titleName, forDate: dateToAdd, location: matchObject.stadium.name, successCompletion: { (message) in
                    self.dismissableAlert(title: titleName, message: message, vc: self, actionBtnText: "Ok".localized(), showCancelButton: false)
                }) { (errorMessage) in
                    self.dismissableAlert(title: "Sorry!😨".localized(), message: errorMessage, vc: self,actionBtnText: "Ok".localized(), showCancelButton: false)
                }
            }, actionBtnText: "Add Match", showCancelButton: true)
            
            
        }
    }
}
