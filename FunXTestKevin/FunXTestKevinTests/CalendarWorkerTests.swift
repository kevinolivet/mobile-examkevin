//
//  CalendarWorkerTests.swift
//  FunXTestKevinTests
//
//  Created by Jon Olivet on 7/31/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import XCTest
@testable import FunXTestKevin

class CalendarWorkerTests: XCTestCase {
    
    var calendarWorker: CalendarWorker!
    override func setUp() {
        super.setUp()
        calendarWorker = CalendarWorker()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        calendarWorker = nil
        super.tearDown()
    }
    
    func testSearchForEventFail() {
        let now = Date()
        let usualMatchTimeInterval = TimeInterval(90 * 60)
        let eventEndDate = now.addingTimeInterval(usualMatchTimeInterval)
        let result = calendarWorker.searchForEvent(withTitle: "Test", forDate: now, endDate: eventEndDate)
        XCTAssertFalse(result)
    }
    
    func testCreateEventinTheCalendarSuccess() {
        let dateToTest = "2018-07-31 00:00:00"
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        guard let eventStartDate = dateFormatterGet.date(from: dateToTest) else { return }
        let usualMatchTimeInterval = TimeInterval(90 * 60)
        let eventEndDate = eventStartDate.addingTimeInterval(usualMatchTimeInterval)
        
        calendarWorker.createEventinTheCalendar(title: "Test", forDate: dateToTest, location: "Here", successCompletion: { (message) in
            
        }) { (errorMessage) in
            
        }
        let result = calendarWorker.searchForEvent(withTitle: "Test", forDate: eventStartDate, endDate: eventEndDate)
        XCTAssertTrue(result)
    }
}
