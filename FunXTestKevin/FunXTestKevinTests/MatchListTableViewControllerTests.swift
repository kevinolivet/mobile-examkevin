//
//  MatchListTableViewControllerTests.swift
//  FunXTestKevinTests
//
//  Created by Jon Olivet on 7/31/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import XCTest
@testable import FunXTestKevin

class MatchListTableViewControllerTests: XCTestCase {
    
    fileprivate var vc: MatchListTableViewController!
    fileprivate var window: UIWindow!
    fileprivate var networkerMock: Networker!
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        self.vc = MatchListTableViewController()
    }
    
    override func tearDown() {
        vc = nil
        window = nil
        super.tearDown()
    }
    
    func loadView() {
        window.addSubview(self.vc.view)
        RunLoop.current.run(until: Date())
    }
    
    func testTableViewSetUp() {
        loadView()
        XCTAssertEqual(vc.imageCache.count, 0)
        XCTAssertEqual(vc.tableView.estimatedRowHeight, 225)
        XCTAssertEqual(vc.tableView.numberOfSections, 1)
        XCTAssertEqual(vc.tableView.numberOfRows(inSection: 0), vc.matchesData.count)
        let indexPath = IndexPath(row: 0, section: 0)
        XCTAssertEqual(vc.tableView(vc.tableView, heightForRowAt: indexPath), UITableViewAutomaticDimension)
    }
    
    func testDidReceiveMemoryWarning() {
        vc.imageCache = ["Test": #imageLiteral(resourceName: "Calendar")]
        XCTAssertEqual(vc.imageCache.count, 1)
        vc.didReceiveMemoryWarning()
        XCTAssertEqual(vc.imageCache.count, 0)
    }
    
    func testGetMoreMatches() {
        // Rewrite with dependency injection
        loadView()
        vc.getMoreMatches()
        XCTAssertEqual(vc.imageCache.count, 0)
    }
    
    func testSetUpRefresh() {
        vc.setUpRefresh()
        XCTAssertNotNil(vc.refreshControl)
    }
    
    func testSetUpSpinner() {
        vc.setUpSpinner()
        XCTAssertEqual(vc.spinner.activityIndicatorViewStyle, .whiteLarge)
        XCTAssertEqual(vc.spinner.color, UIColor.black)
        XCTAssertTrue(vc.spinner.isAnimating)
        XCTAssertTrue(vc.spinner.hidesWhenStopped)
        XCTAssertEqual(vc.tableView.backgroundView, vc.spinner)
    }
    
}
