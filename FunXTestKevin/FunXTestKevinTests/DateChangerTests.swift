//
//  DateChangerTests.swift
//  FunXTestKevinTests
//
//  Created by Jon Olivet on 7/31/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import XCTest
@testable import FunXTestKevin

class DateChangerTests: XCTestCase {
    
    fileprivate var dateChanger: DateChanger!
    
    override func setUp() {
        super.setUp()
        dateChanger = DateChanger()
    }
    
    override func tearDown() {
        dateChanger = nil
        super.tearDown()
    }
    
    func testDateFromEndpointToLabelSuccess() {
        let stringToChange = "2018-02-02"
        let changedDate = dateChanger.dateFromEndpointToLabel(dateToChange: stringToChange)
        XCTAssertEqual(changedDate, "02 de Feb.")
    }
    
    func testDateFromEndpointToLabelFail() {
        let stringToChange = "February 2, 2018"
        let changedDate = dateChanger.dateFromEndpointToLabel(dateToChange: stringToChange)
        XCTAssertEqual(changedDate, "")
    }
}
