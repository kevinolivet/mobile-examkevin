//
//  FunXTestKevinUITests.swift
//  FunXTestKevinUITests
//
//  Created by Jon Olivet on 7/30/18.
//  Copyright © 2018 Jon Olivet. All rights reserved.
//

import XCTest

class FunXTestKevinUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDown() {
        app = nil
        super.tearDown()
    }
    
    func testLoadSuccess() {
        let label = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"02 de Feb. - 22:00hrs -  Mansiche")/*[[".cells.containing(.staticText, identifier:\"Sporting Cristal\")",".cells.containing(.staticText, identifier:\"UTC\")",".cells.containing(.staticText, identifier:\"02 de Feb. - 22:00hrs -  Mansiche\")"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["0 - 0"]
        XCTAssertNotNil(label)
    }
    
    func testScroll() {
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"03 de Feb. - 22:00hrs -  Miguel Grau")/*[[".cells.containing(.staticText, identifier:\"Sport Boys\")",".cells.containing(.staticText, identifier:\"03 de Feb. - 22:00hrs -  Miguel Grau\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["0 - 0"].swipeUp()
    }
    
    func testAddToCalendarCancel() {
        app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"02 de Feb. - 22:00hrs -  Mansiche")/*[[".cells.containing(.staticText, identifier:\"Sporting Cristal\")",".cells.containing(.staticText, identifier:\"UTC\")",".cells.containing(.staticText, identifier:\"02 de Feb. - 22:00hrs -  Mansiche\")"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["Button"].tap()
        let alert = app.alerts["Add to Calendar?"].buttons["Cancel"]
        XCTAssertNotNil(alert)
    }
    
    func testAddToCalendarAccept() {
        app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"02 de Feb. - 22:00hrs -  Mansiche")/*[[".cells.containing(.staticText, identifier:\"Sporting Cristal\")",".cells.containing(.staticText, identifier:\"UTC\")",".cells.containing(.staticText, identifier:\"02 de Feb. - 22:00hrs -  Mansiche\")"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["Button"].tap()
        app.alerts["Add to Calendar?"].buttons["Add Match"].tap()
        let sorry = app.alerts["Sorry!😨"].buttons["Ok"]
        XCTAssertNotNil(sorry)
    }
}
